<?php
/* @var $this yii\web\View */

/* @var $model frontend\modules\post\models\forms\PostForm */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<div class="post-default-index">

    <h1>Выберите фото</h1>

    <?php $form = ActiveForm::begin(['class' => 'form-group']); ?>

    <?php echo $form->field($model, 'picture')->fileInput(['class' => "form-control-file"]); ?>

    <?php echo $form->field($model, 'description'); ?>

    <?php echo Html::submitButton('Загрузить', ['class' => 'btn btn-outline-primary']); ?>

    <?php ActiveForm::end(); ?>

</div>
